package org.hsb.productinfoapi.service;

import lombok.RequiredArgsConstructor;
import org.hsb.productinfoapi.entity.ProductInfo;
import org.hsb.productinfoapi.model.CreateProductInfoRequest;
import org.hsb.productinfoapi.model.ProductInfoItem;
import org.hsb.productinfoapi.model.ProductInfoResponse;
import org.hsb.productinfoapi.repository.ProductInfoRepository;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ProductInfoService {
    private final ProductInfoRepository productInfoRepository;

    public void setProductInfo(CreateProductInfoRequest request){
        ProductInfo addData = new ProductInfo();
        addData.setImageName(request.getImageName());
        addData.setProductName(request.getProductName());
        addData.setPrice(request.getPrice());

        productInfoRepository.save(addData);
    }

    public List<ProductInfoItem> getProductInfos(){
        List<ProductInfo> originData = productInfoRepository.findAll();
        List<ProductInfoItem> result = new LinkedList<>();

        for (ProductInfo productInfo : originData){
            ProductInfoItem addItem = new ProductInfoItem();
            addItem.setId(productInfo.getId());
            addItem.setImageName(productInfo.getImageName());
            addItem.setProductName(productInfo.getProductName());
            addItem.setPrice(productInfo.getPrice());

            result.add(addItem);
        }
        return result;
    }

    public ProductInfoResponse getProductInfo(long id){
        // 창고지기한테 id번의 데이터를 가져다 달라고 요청한다. --> 원본요청
        ProductInfo productInfo = productInfoRepository.findById(id).orElseThrow();

        // 새로 옮겨 닮을 그릇을 준비한다.
        ProductInfoResponse response = new ProductInfoResponse();
        response.setId(productInfo.getId()); // 새로 옮겨 담을 그릇의 id칸에 원본에서 id값을 빼내와서 옮겨담는다.
        response.setImageName(productInfo.getImageName());
        response.setProductName(productInfo.getProductName());
        response.setPrice(productInfo.getPrice());

        return response; // 그릇에 다 옮겨 담았으니 새로운 그릇을 돌려준다.
    }
}
