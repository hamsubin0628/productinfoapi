package org.hsb.productinfoapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonResult {
    private String msg; //인사 메세지
    private Integer code; //코드
}
