package org.hsb.productinfoapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductInfoResponse {
    private Long id;
    private String imageName;
    private String productName;
    private Double price;
}
