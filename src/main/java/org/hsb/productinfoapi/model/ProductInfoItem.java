package org.hsb.productinfoapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductInfoItem {
    private Long id;
    private String imageName;
    private String productName;
    private Double price;
}
