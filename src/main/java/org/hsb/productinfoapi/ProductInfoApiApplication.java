package org.hsb.productinfoapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductInfoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductInfoApiApplication.class, args);
	}

}
