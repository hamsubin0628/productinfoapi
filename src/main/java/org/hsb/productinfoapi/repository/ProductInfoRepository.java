package org.hsb.productinfoapi.repository;

import org.hsb.productinfoapi.entity.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductInfoRepository extends JpaRepository<ProductInfo, Long> {
}
