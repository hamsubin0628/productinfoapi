package org.hsb.productinfoapi.controller;

import lombok.RequiredArgsConstructor;
import org.hsb.productinfoapi.model.*;
import org.hsb.productinfoapi.service.ProductInfoService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/product-info")
public class ProductInfoController {
    private final ProductInfoService productInfoService;

    @PostMapping("/new")
    public String setProductInfo(@RequestBody CreateProductInfoRequest request){
        productInfoService.setProductInfo(request);

        return "ok";
    }

    @GetMapping("/all")
    public ListResult<ProductInfoItem> getProductInfos(){
        List<ProductInfoItem> list = productInfoService.getProductInfos();

        ListResult<ProductInfoItem> response = new ListResult<>();
        response.setList(list);
        response.setTotalCount(list.size());
        response.setCode(0);
        response.setMsg("성공하였습니다.");
        return response;
    }

    @GetMapping("/detail/{id}")
    public SingleResult<ProductInfoResponse> getDetail(@PathVariable long id){
        ProductInfoResponse result = productInfoService.getProductInfo(id);

        SingleResult<ProductInfoResponse> response = new SingleResult<>();
        response.setMsg("성공하였습니다.");
        response.setCode(0);
        response.setData(result);

        return response;
    }
}
