package org.hsb.productinfoapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class ProductInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 30)
    private String imageName;

    @Column(nullable = false, length = 30)
    private String productName;

    @Column(nullable = false)
    private Double price;
}
